import os
import csv
from datetime import datetime, date, time, timedelta

CHECKIN, CHECKOUT = '0', '1'

def loadMDBtoCSV(dbfile, tableName):
    mdb = os.popen('/usr/bin/mdb-export %s %s' % (dbfile, tableName))
    csvreader = csv.reader(mdb)
    return csvreader

def createDictListFromMDB(dbfile, tableName):
    dictlist = []
    csvreader = loadMDBtoCSV(dbfile, tableName)
    header = csvreader.next()
    for row in csvreader:
        data = {}
        for i in xrange(len(header)):
            data[header[i]] = row[i]
        dictlist.append(data)
    return dictlist

def absensiFromMDB(database, jangkaWaktu=timedelta(minutes=30)):
    data = []
    data = createDictListFromMDB(database, 'PersonalLog')
    karyawan = createDictListFromMDB(database, 'Employee')
    department = createDictListFromMDB(database, 'Department')
    out = {}

    for row in data:
        emp = filter(lambda x: x['FingerPrintID'] == row['FingerPrintID'], 
            karyawan)
        if len(emp) >= 1:
            emp = emp[0]
            dept = filter(lambda x: x['DepartmentCode'] == emp['DepartmentCode'], 
                department)[0]['DepartmentName']
            employeeID = '%s:%s:%s' % (row['FingerPrintID'], '%s %s %s' % 
                (emp['EmployeeFirstName'], emp['EmployeeMiddleName'], 
                 emp['EmployeeLastName']), dept)
        else:
            employeeID = row['FingerPrintID']
        if employeeID not in out:
            out[employeeID] = []
        waktu = (datetime.strptime(row['DateTime'], '%m/%d/%y %H:%M:%S'), 
            row['FunctionKey'])
        out[employeeID].append(waktu)

    table = {}

    # jangka waktu 30 menit
    jangka = jangkaWaktu

    for employee, timelog in out.iteritems():
        timelog.sort(key=lambda x: x[0])
        if employee not in table:
            table[employee] = []
        for t in timelog:
            if t[0].time() < time(12,0,0):
                #cari waktu dalam rentang 30 menit sebelum dan setelah waktu ini, kalau jumpa bandingkan ambil yang paling kecil
                waktu = [x for x in table[employee] if x[0] is not None and x[0] >= t[0] - jangka and x[0] <= t[0]+jangka]
                if len(waktu) > 0:
                    waktu = waktu[0]
                    waktu = [ t[0] if t[0] < waktu[0] else waktu[0], waktu[1] ] 
                else:
                    waktu = [ t[0], None ]
                    table[employee].append(waktu)
            else:
                #cari waktu checkin yang paling mendekati waktu checkout
                waktu = [(x, t[0] - x[0]) for x in table[employee] if x[0] is not None 
                    and x[0] < t[0] 
                    and t[0]-x[0] < timedelta(hours=24)]
                if len(waktu) > 0:
                    waktu = max(waktu, key=lambda x: x[1])
                    if waktu[0][1] is None:
                        waktu[0][1] = t[0]
                    else:
                        waktu[0][1] = waktu[0][1] if waktu[0][1] > t[0] else t[0]
                else:
                    #print t[0], 'no checkin time?'
                    table[employee].append([None, t[0]])
    return table

def absensiToCSV(table, out):
    csvwriter = csv.writer(out)
    for i, j in table.iteritems():
        #print i
        for k in j:
            #print k[0], k[1], None if k[1] is None else k[1]-k[0]
            keys = i.split(':')
            if (len(keys) == 3):
                csvwriter.writerow([keys[0], keys[1], keys[2], k[0], k[1]])
            else:
                csvwriter.writerow([keys[0], '', '', k[-1], k[1]])

def absensiToList(table):
    l = []
    for i, j in table.iteritems():
        for k in j:
            keys = i.split(':')
            if (len(keys) == 3):
                l.append({'id': keys[0], 'nama': keys[1], 'department': keys[2], 'masuk': k[0], 'keluar': k[1]})
            else:
                l.append({'id': keys[0], 'nama': '', 'department': '', 'masuk': k[0], 'keluar': k[1]})

    l.sort(key=lambda x: x['nama'])
    return l


if __name__ == '__main__':
    absensiToCSV(absensiFromMDB('HITFPTA_TAL.mdb'), open('out.csv', 'w'))
