import json
import datetime

class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            #return obj.strftime('%Y-%m-%d %H:%M:%S')
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)
