#!/usr/bin/env python

import os
import pickle
from StringIO import StringIO
import json
import datetime

import xlwt
from xlwt import easyxf

from flask import Flask
from flask import request
from flask import render_template
from flask import abort
from flask import make_response
from werkzeug import secure_filename
from beaker.middleware import SessionMiddleware as BeakerSession

import absensi
from jsonencoder import DateTimeEncoder

class MyStringIO(StringIO):
    def close(self):
        pass

    def _close(self):
        StringIO.close(self)

app = Flask(__name__)
session_opts = {'session.data_dir'      : './sessions/data',
                'session.lock_dir'      : './sessions/lock',
                'session.type'          : 'file',
                'session.auto'          : True,
                'session.cookie_expires': True,
                'session.key'           : 'qDzGqlzgIHhegoFwfrgFfqAjzzFuoICcBegwJbkpGdotgGwtFwAmdsfnfJwwwzjDxoawlvuwkoryczgDcmwamgHuvplelfGexuqJdIIxGHaAymsbmatwHfaptzeoqFox',
                'session.secret'        : 'pdoqEDadIBfiybblJegdkokCcfrfmfvfIoEhhtclcIsCfwnHdFzeIxaAHttgdgetJCvzEkJburlFCegAubsEtGyBxzakIDngnFzntbtsmzpFIqvmklJxqwmnddybqiza'}

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('query.html', 
            tahun=datetime.datetime.now().year)
    elif request.method == 'POST':
        session = request.environ['beaker.session']
        berkas = request.files.get('berkas', None)
        if berkas is None:
            abort(400)

        if getattr(berkas, 'save', None) is None:
            abort(400)

        tmpname = os.tmpnam()

        berkas.save(tmpname)

        try:
            out = absensi.absensiToList(absensi.absensiFromMDB(tmpname))
        except:
            return Response(status='400 Bad Request', text=u'Jenis file salah')
        os.unlink(tmpname)
        session['absensi'] = out
        session.save()

        pegawai = list(set([ x['nama'] for x in session['absensi'] ]))
        pegawai.sort()

        department = list(set([ x['department'] for x in session['absensi'] ]))
        department.sort()

        return render_template('query.html', tahun=datetime.datetime.now().year,
            absensi=session['absensi'], pegawai=pegawai, department=department)



@app.route('/xhr/excel', methods=['GET'])
def exportToExcel():
    caption = {}
    for i in ('nama', 'tahun', 'bulan'):
        caption[i] = request.args.get(i, '')

    session = request.environ['beaker.session']

    try:
        tahun = int(caption['year'])
        #hasil = [ x for x in session['absensi'] ]
        hasil = filter(lambda x: (x['masuk'] if x['masuk'] is not None 
            else x['keluar']).year == tahun, session['absensi'])
    except:
        hasil = session['absensi']

    try:
        bulan = int(caption['bulan'])
        if bulan in xrange(1, 13):
            hasil = filter(lambda x: (x['masuk'] if x['masuk'] is not None 
                else x['keluar']).month == bulan, session['absensi'])
    except:
        pass

    try:
        nama = caption['nama']
        if nama != '':
            hasil = [ x for x in hasil if x['nama'] == nama ]
    except:
        pass

    wb = xlwt.Workbook()
    sheet = wb.add_sheet('Sheet1')

    sheet.col(0).width = 3000
    sheet.col(1).width = 10000
    sheet.col(2).width = 5000 
    sheet.col(3).width = 5000
    sheet.col(4).width = 5000

    label = ('Tanggal', 'Nama', 'Department', 'Masuk', 'Keluar')
    for i in xrange(len(label)):
        sheet.write(0, i, label[i])

    for i in xrange(len(hasil)):
        sheet.row(i+1).write(0, hasil[i]['masuk'].date() if hasil[i]['masuk'] 
            is not None else hasil[i]['keluar'].date(), 
            easyxf(num_format_str='YYYY-MM-DD'))
        sheet.row(i+1).write(1, hasil[i]['nama'])
        sheet.row(i+1).write(2, hasil[i]['department'])
        if hasil[i]['masuk']:
            sheet.row(i+1).set_cell_date(3, hasil[i]['masuk'], 
            easyxf(num_format_str='HH:MM:SS'))
        if hasil[i]['keluar']:
            sheet.row(i+1).set_cell_date(4, hasil[i]['keluar'], 
            easyxf(num_format_str='HH:MM:SS'))

    print caption['nama']

    output = MyStringIO()
    wb.save(output)

    response = make_response(output.getvalue())
    response.headers['Content-Type'] = 'application/ms-excel'
    response.headers['Content-Disposition'] = 'attachment; filename="absensi.xls"'

    output._close()

    return response

if __name__ == '__main__':
    app.wsgi_app = BeakerSession(app.wsgi_app, session_opts)
    app.run(debug=True)
